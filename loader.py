import pathlib


def load_clauses(file_name):
    path = str(pathlib.Path(__file__).parent.resolve()) + '/' + file_name + '.txt'
    nbvar = 0
    nbclauses = 0
    first = []
    second = []
    with open(path) as f:
        lines = f.readlines()
        first_line = lines[0].split(' ')
        nbvar = int(first_line[0])
        nbclauses = int(first_line[1].replace('\n', ''))
        for line in lines[1:]:
            line_variables = list(map(int, line.split(' ')[:-1]))
            first.append(line_variables[0])
            second.append(line_variables[1] if len(line_variables) >= 2 else None)
    return nbvar, nbclauses, first, second


def get_graph(nbvar, nbclauses, first, second):
    # graph representation
    adj = [[] for _ in range(nbvar * 2)]
    adj_inv = [[] for _ in range(nbvar * 2)]

    for i in range(nbclauses):
        if second[i] is not None:
            first_b = first[i] > 0
            second_b = second[i] > 0
            add_disjunction(abs(first[i]), first_b, abs(second[i]), second_b, adj, adj_inv)

    return adj, adj_inv


def add_disjunction(first: int, first_b: bool, second: int, second_b: bool, adj: [], adj_inv: []):
    a = 2 * first ^ first_b
    b = 2 * second ^ second_b
    neg_a = a ^ 1
    neg_b = b ^ 1
    adj[neg_a - 2].append(b - 2)
    adj[neg_b - 2].append(a - 2)
    adj_inv[b - 2].append(neg_a - 2)
    adj_inv[a - 2].append(neg_b - 2)
