class Solver2Sat(object):
    def __init__(self, adj, adj_inv, nbvar):
        self.adj = adj
        self.adj_inv = adj_inv
        self.nodes_count = nbvar * 2
        self.used = []
        self.order = []
        self.components = []
        self.assignment = []

    def solve_problem(self):
        # calculate order of nodes which will help with strong connected components
        self.order = []
        self.used = [False for _ in range(self.nodes_count)]
        for node_index in range(self.nodes_count):
            if self.used[node_index] is False:
                self.__dfs1(node_index)
        print('ORDER: ', self.order)

        # calculate strong connected components
        self.components = [-1 for _ in range(self.nodes_count)]
        component_number = 0
        # iterate ordered nodes from end
        for node_index in reversed(self.order):
            if self.components[node_index] == -1:
                self.__dfs2(node_index, component_number)
                component_number += 1
        print('COMPONENTS: ', self.components)

        # test if x and -x are not in same component
        print('\n----------------------')
        self.assignment = [False for _ in range(int(self.nodes_count / 2))]
        for i in range(0, self.nodes_count, 2):
            if self.components[i] == self.components[i + 1]:
                print('NESPLNITEĽNÁ')
                return
            self.assignment[int(i / 2)] = self.components[i] > self.components[i + 1]

        print('SPLNITEĽNÁ')
        for clause_result in self.assignment:
            if clause_result is True:
                print('PRAVDA')
            else:
                print('NEPRAVDA')

    def __dfs1(self, node_index):
        self.used[node_index] = True
        for initial_node in self.adj[node_index]:
            if self.used[initial_node] is False:
                self.__dfs1(initial_node)
        self.order.append(node_index)

    def __dfs2(self, node_index, component_number):
        self.components[node_index] = component_number
        for initial_node in self.adj_inv[node_index]:
            if self.components[initial_node] == -1:
                self.__dfs2(initial_node, component_number)
