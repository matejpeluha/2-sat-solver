from loader import load_clauses, get_graph
from solver import Solver2Sat

nbvar, nbclauses, first, second = load_clauses('clauses3')
print('NUMBER OF VARIABLES: ', nbvar)
print('NUMBER OF CLAUSES: ', nbclauses)
print('FIRST: ', first)
print('SECOND: ', second)


adj, adj_inv = get_graph(nbvar, nbclauses, first, second)

print('ADJ: ', adj)
print('ADJ inv: ', adj_inv)

solver = Solver2Sat(adj, adj_inv, nbvar)
solver.solve_problem()
